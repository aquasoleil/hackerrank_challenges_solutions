/*
    Given a 6 X 6 2D Array, arr:
    [
        [1, 1, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ];
    We define an hourglass in A to be a subset of values with indices
    falling in this pattern in arr's graphical representation:
        abc
         d
        efg
    There are 16 hourglasses in arr, and an hourglass sum is the sum
    of an hourglass' values. Calculate the hourglass sum for every hourglass in arr,
    then print the maximum hourglass sum.
*/

const hourglassSum = arr => {
    let maxHourglassSum = Number.MIN_SAFE_INTEGER;
    for (let vertical = 0; vertical < arr.length - 2; vertical++) {
        for (let horisontal = 0; horisontal < arr.length - 2; horisontal++) {
            const hourglass = [
                arr[vertical][horisontal], arr[vertical][horisontal + 1], arr[vertical][horisontal + 2],
                arr[vertical + 1][horisontal + 1],
                arr[vertical + 2][horisontal], arr[vertical + 2][horisontal + 1], arr[vertical + 2][horisontal + 2]
            ];
            const reducedHourglass = hourglass.reduce((acc, current) => acc + current);
            if (reducedHourglass > maxHourglassSum) {
                maxHourglassSum = reducedHourglass;
            }
        }
    }
    return maxHourglassSum;
};

module.exports = hourglassSum;