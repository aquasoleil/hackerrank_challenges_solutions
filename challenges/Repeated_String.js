/*
    Lilah has a string, s, of lowercase English letters that she repeated infinitely many times.
    Given an integer, n, find and print the number of letter a's in the first n letters of Lilah's infinite string.
    For example, if the string s = 'abcac' and n = 10, the substring we consider is 'abcacabcac',
    the first 10 characters of her infinite string. There are 4 occurrences of a in the substring.
*/

const repeatedString = (s, n) => {
    const A_CHARACTER = 'a';
    let count = 0;
    if (s.includes(A_CHARACTER)) {
        if (s.length === 1) {
            return n;
        }
        const numberOfAs = substring => {
            let numberOfAsInSubstring = 0;
            for (let index = 0; index < substring.length; index++) {
                if (A_CHARACTER === substring.charAt(index)) {
                    numberOfAsInSubstring ++;
                }
            }
            return numberOfAsInSubstring;
        };
        const numberOfRepetitions = Math.floor(n / s.length);
        const numberOfLeftoverCharacters = n - numberOfRepetitions * s.length;
        const numberOfAsInMajorString = numberOfAs(s) * numberOfRepetitions;
        const leftOverString = s.slice(0, numberOfLeftoverCharacters);
        count = numberOfAsInMajorString + numberOfAs(leftOverString);
    }
    return count;
};

module.exports = repeatedString;