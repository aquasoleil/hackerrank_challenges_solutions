const Sock_Merchant = require('../challenges/Sock_Merchant');

describe('Sock_Merchant', () => {
    test('returns an integer representing the number of matching pairs of socks that are available', () => {
        const socks = [1, 2, 3, 1, 2, 2, 3, 1, 1, 2, 2];
        expect(Sock_Merchant(socks)).toBe(5);
    });
});