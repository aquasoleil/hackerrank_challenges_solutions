const Jumping_on_the_Clouds = require('../challenges/Jumping_on_the_Clouds');

describe('Jumping_on_the_Clouds', () => {
    test('returns the minimum number of jumps required, as an integer', () => {
        clouds = [0, 0, 1, 0, 0, 1, 0];
        expect(Jumping_on_the_Clouds(clouds)).toBe(4);
    });
});