const Repeated_String = require('../challenges/Repeated_String');

describe('Repeated_String', () => {
    test('returns an integer representing the number of occurrences of a in the prefix of length n', () => {
        const stringToRepeat = 'aba';
        const numberOfFirstLetters = 10;
        expect(Repeated_String(stringToRepeat, numberOfFirstLetters)).toBe(7);
    });
});