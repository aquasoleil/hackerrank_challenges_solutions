const Counting_Valleys = require('../challenges/Counting_Valleys');

describe('Counting_Valleys', () => {
    test('returns an integer that denotes the number of valleys traversed', () => {
        const terrain = ['U', 'D', 'D', 'D', 'U', 'U'];
        expect(Counting_Valleys(terrain)).toBe(1);
    });
});